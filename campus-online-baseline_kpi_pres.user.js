// ==UserScript==
// @name       Pass All Baselines and KPIs
// @namespace  http://use.i.E.your.homepage/
// @version    0.1
// @description  enter something useful
// @match      https://moodle-brisbane.axis.navitas.com/*grading*
// @copyright  2014+, Steve Halliwell
// @require     http://code.jquery.com/jquery-latest.js
// ==/UserScript==


$(document).ready(function(){
    
    //handle baselines
    var baseRes = $("section div h2:contains('Baseline Assessment')");
    var KPIRes = $("section div h2:contains('KPI Assessment')");
    var presRes = $("section div h2:contains('Presentation')");
    
    if(baseRes.length != 0){
        $("td.c13 div select").val("2");
        $("td.c12 div select").val("2");
    }
    
    //handle KPIs
    if(KPIRes.length != 0 || presRes.length != 0 || baseRes.length != 0){
        //ok lets default EVERYTHING to complete
        $("td.c5 select").val("released");
        $("td.c6 select").val("2");
    }
});