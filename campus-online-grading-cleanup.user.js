// ==UserScript==
// @name       Clean up Campus Online
// @namespace  http://use.i.E.your.homepage/
// @version    0.1
// @description  enter something useful
// @match      https://moodle-brisbane.axis.navitas.com/*grading*
// @copyright  2014+, Steve Halliwell
// @require     http://code.jquery.com/jquery-latest.js
// ==/UserScript==

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

function killColumnIf(thClass, containsText, newStyle)
{
    globalCSSIfCol(thClass, containsText, "{ display:none !important; }");
}

function widthOfColumnIf(thClass, containsText, width)
{
    globalCSSIfCol(thClass, containsText, "".concat("{ width: ",width,"px !important; max-width: ",width,"px !important; white-space:normal !important;}"));
}

function globalCSSIfCol(thClass, containsText, newStyle)
{
    if( $( "th".concat(thClass) ).text().indexOf(containsText) != -1)
    {
         addGlobalStyle( thClass.concat(newStyle) );
    }
}

$(document).ready(function(){
    
    //your fake folders and icons waste precious space
    addGlobalStyle('.ygtvln,.ygtvtn,img.icon { display:none !important; }');
    addGlobalStyle('.span9 { width:100% !important; }');
    
    //kill the columns that are useless info
    	//dups are seperate for a reason, as dual selector will become an or, we don't want that
    killColumnIf(".c1", "User picture");
    killColumnIf(".c3", "ID number");
    killColumnIf(".c4", "Email address");
    killColumnIf(".c6", "Marker");
    killColumnIf(".c8", "Last modified");
    killColumnIf(".c9", "Last modified");
    killColumnIf(".c10", "Submission comments");
    killColumnIf(".c11", "Submission comments");
    killColumnIf(".c11", "Last modified");
    killColumnIf(".c12", "Last modified");
    killColumnIf(".c10", "Annotate");
    killColumnIf(".c13", "Annotate");
    killColumnIf(".c14", "Annotate");
    killColumnIf(".c11", "Final grade");
    killColumnIf(".c12", "Final grade");
    killColumnIf(".c15", "Final grade");
    
    
    //set logical limits of the width of columns
    widthOfColumnIf(".c0","Select",20);
    widthOfColumnIf(".c2","First name",60);
    widthOfColumnIf(".c5","Status",80);
    widthOfColumnIf(".c6","Grade",80);
    widthOfColumnIf(".c7","Grade",80);
    widthOfColumnIf(".c7","Edit",40);
    widthOfColumnIf(".c8","Edit",40);
    widthOfColumnIf(".c9","File sub",80);
    widthOfColumnIf(".c10","File sub",80);
    widthOfColumnIf(".c11","Feedback files",80);
	
	globalCSSIfCol(".c9","File sub","{overflow:auto !important;}");
	globalCSSIfCol(".c10","File sub","{overflow:auto !important;}");
    
    //comments box should be large as possible
    addGlobalStyle('textarea.quickgrade { width: 98% !important;  height: 98% !important;}');
    
});