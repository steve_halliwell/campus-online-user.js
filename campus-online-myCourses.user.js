// ==UserScript==
// @name       Clean up Campus Online course list
// @namespace  http://use.i.E.your.homepage/
// @version    0.2
// @description  removes all units from main course list that are archived
// @match      https://moodle-brisbane.axis.navitas.com/my/
// @copyright  2014+, Steve Halliwell
// @require     http://code.jquery.com/jquery-latest.js
// ==/UserScript==


$(document).ready(function(){
    $(".coursebox .course_title h2 a").each(function() {
  		if($(this).text().indexOf("archived") != -1)
        {
            $(this).css("color","#A0A0A0");
            $(this).parent().parent().parent().css("display", "none");
      	}
    });
});